import help from '@/config/form'


const mask = '####-####-####-####'

// const items = [{ value: 1, text: 'Option A' }, { value: 2, text: 'Option b' }, { value: 3, text: 'Option c' }, { value: 4, text: 'Option d' }]

const statusItems = ["Pending", "Confirmed", "Accepted", "In Transit", "Destination Reached", "Work Started","Work Completed"
                    , "Cancelled", "Closed", "Left-of"]

const paymentItems = ["Cash", "Card"]

const mySchema = {
    // schema prop: string 'text' => shorthand for prop: { type: 'text' }        
    customer: {
        type: 'text',
        label: 'Customer',
        flex: { xs: 12, sm: 6, md: 6 },
        disabled: true,
    },
    provider: {
        type: 'text',
        label: 'Provider',
        flex: { xs: 12, sm: 6, md: 6 },
        disabled: true,
    },
    status: { type: 'select', label: 'Status', items: statusItems, },

    payment_method: { type: 'select', label: 'Payment Method', items: paymentItems, disabled: true},

    total_qty: {
        type: 'number',
        label: 'Totsl Disance(Km)',
        flex: { xs: 12, sm: 6, md: 6 },
        disabled: true,
    },
    total: {
        type: 'number',
        label: 'Total Amount(AED)',
        flex: { xs: 12, sm: 3, md: 3, lg: 3 },
        disabled: true,
    },
    discount_amount: {
        type: 'number',
        label: 'Discounted  Amount(AED)',
        flex: { xs: 12, sm: 3, md: 3, lg: 3 },
        disabled: true,
    },
    net_total: {
        type: 'number',
        label: 'Net Amount(AED)',
        flex: { xs: 12, sm: 3, md: 3, lg: 3 },
        disabled: true,
    },
    rounding_adjustment: {
        type: 'number',
        label: 'Rounding Adjustment(AED)',
        flex: { xs: 12, sm: 3, md: 3, lg: 3 },
        disabled: true,
    },
    rounded_total: {
        type: 'number',
        label: 'Rounded Total(AED)',
        flex: { xs: 12, sm: 3, md: 3, lg: 3 },
        disabled: true,
    },
    grand_total: {
        type: 'number',
        label: 'Grand Total(AED)',
        flex: { xs: 12, sm: 3, md: 3, lg: 3 },
        disabled: true,
    },
    commission_rate: {
        type: 'number',
        label: 'Commission Rate(%)',
        flex: { xs: 12, sm: 3, md: 3, lg: 3 },
        disabled: true,
    },
    total_commission: {
        type: 'number',
        label: 'Total Commission(AED)',
        flex: { xs: 12, sm: 3, md: 3, lg: 3 },
        disabled: true,
    },
    driver_pay: {
        type: 'number',
        label: 'Total Driver Payment(AED)',
        flex: { xs: 12, sm: 3, md: 3, lg: 3 },
        disabled: true,
    },
    ride_date: {
        type: 'text',
        label: 'Ride Date',
        flex: { xs: 12, sm: 6, md: 6, lg: 6 },
        disabled: true,
    },
    ride_type: {
        type: 'text',
        label: 'Ride Type',
        rules: [help.required('Emirates ID is required')],
        flex: { xs: 12, sm: 6, md: 6, lg: 6 },
        disabled: true,
    },
}


export default mySchema
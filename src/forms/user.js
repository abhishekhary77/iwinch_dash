import help from '@/config/form'


const mask = '####-####-####-####'

// const items = [{ value: 1, text: 'Option A' }, { value: 2, text: 'Option b' }, { value: 3, text: 'Option c' }, { value: 4, text: 'Option d' }]

const mySchema = {
    // schema prop: string 'text' => shorthand for prop: { type: 'text' }        
    first_name: {
        type: 'text',
        label: 'First Name',
        rules: [help.required('First Name is required<>')],
        flex: 12,
    },
    second_name: {
        type: 'text',
        label: 'Second Name',
        flex: { xs: 12, sm: 6, md: 6 },
    },
    email_id: {
        type: 'email',
        label: 'Email',
        rules: [help.validEmail('No valid Email')],
        flex: { xs: 12, sm: 6, md: 6 },
    },
    // name: { type: 'autocomplete', label: 'names', items: items, },
}

export default mySchema
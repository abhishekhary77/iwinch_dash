import help from '@/config/form'


const mask = '####-####-####-####'

// const items = [{ value: 1, text: 'Option A' }, { value: 2, text: 'Option b' }, { value: 3, text: 'Option c' }, { value: 4, text: 'Option d' }]

const mySchema = {
    // schema prop: string 'text' => shorthand for prop: { type: 'text' }        
    first_name: {
        type: 'text',
        label: 'First Name',
        rules: [help.required('First Name is required')],
        flex: { xs: 12, sm: 6, md: 6 },
        disabled: true,
    },
    second_name: {
        type: 'text',
        label: 'Second Name',
        flex: { xs: 12, sm: 6, md: 6 },
        disabled: true,
    },
    email_id: {
        type: 'email',
        label: 'Email',
        rules: [help.validEmail('No valid Email')],
        flex: { xs: 12, sm: 6, md: 6 },
        disabled: true,
    },
    vehicle_number: {
        type: 'text',
        label: 'Vehicle ID',
        rules: [help.required('Vehicle ID is required')],
        flex: { xs: 12, sm: 3, md: 3, lg: 3 },
        disabled: true,
    },
    license_no: {
        type: 'text',
        label: 'License ID',
        rules: [help.required('License ID is required')],
        flex: { xs: 12, sm: 3, md: 3, lg: 3 },
        disabled: true,
    },
    visa_no: {
        type: 'text',
        label: 'Visa ID',
        rules: [help.required('Visa ID is required')],
        flex: { xs: 12, sm: 6, md: 6, lg: 6 },
        disabled: true,
    },
    em_id: {
        type: 'text',
        label: 'Emirates ID',
        rules: [help.required('Emirates ID is required')],
        flex: { xs: 12, sm: 6, md: 6, lg: 6 },
        disabled: true,
    },
    acc_no: {
        type: 'text',
        label: 'Account Number',
        rules: [help.required('Account Number is required')],
        flex: { xs: 12, sm: 6, md: 6, lg: 6 },
        disabled: true,
    },
}


export default mySchema
import Vue from 'vue'
import api from '@/config/api'

const getUrl = (url) => {
  let urlArray = []
  if (api.api) {
    urlArray.push(api.api)
  } else {
    urlArray.push(api.url)
    if (api.path.prefix) {
      urlArray.push(api.path.prefix)
    }
  }
  if (api.prefix) {
    urlArray.push(api.prefix)
  }
  urlArray.push(url)
  return urlArray.join('/')
}

const actions = {
  get_drivers ({ commit }, data) {
    commit('set_loading', true)
    return new Promise((resolve) => {
      const path = api.path.drivers 
      Vue.http.post(getUrl(path),data)
        .then(response => response.json())
        .then((result) => {
          commit('set_drivers', result)
          resolve()
        })
        .catch(() => {
          commit('set_loading', false)
        })
    })
  },
  getDriver ({ commit, state}) {
    commit('set_userloaded', false)
    return new Promise((resolve) => {
      let data = {"name": state.selected_driver}
      const path = api.path.getDriver
      Vue.http.post(getUrl(path), data)
        .then(response => response.json())
        .then((result) => {
          commit('set_driver', result)
          resolve()
        })
        .catch(() => {
          commit('set_loading', false)
        })
    })
  },
  get_orders ({ commit, state}, data) {
    commit('set_loading', true)
    return new Promise((resolve) => {
      const path = api.path.completedorders
      data.filters.provider = state.selected_driver
      data.filters = JSON.stringify(data.filters)
      Vue.http.post(getUrl(path),data)
        .then(response => response.json())
        .then((result) => {
          console.log(result)
          commit('set_order', result)
          resolve()
        })
        .catch(() => {
          commit('set_loading', false)
        })
    })
  },
  updateDriver({ commit, state}, data) {
    commit('set_userloaded', false)
    return new Promise((resolve) => {
      const path = api.path.updateDriver
      console.log('data data data data')
      console.log(path);
      Vue.http.post(getUrl(path),data)
        .then(response => response.json())
        .then((result) => {
          console.log(result)
          commit('set_driver', result)
          resolve()
        })
        .catch(() => {
          commit('set_loading', false)
        })
    })
  },
}

export default actions

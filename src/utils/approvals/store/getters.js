const getters = {
  approvalsInfo: state => state.approvals,
  isloading: state => state.isLoading,
  totalRecords: state => state.total_records,
  driverDetail: state => {
  return JSON.parse(state.driver_details) || {
    active: null,
    email: null,
    name: null,
    }
  },
  isuserloaded: state => state.userloaded,
}

export default getters

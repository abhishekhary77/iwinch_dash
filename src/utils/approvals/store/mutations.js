const mutations = {
  set_approvals (state, data) {
    const approvals  = data.message.drivers
    state.approvals = approvals
    state.isLoading = false
    state.total_records = data.message.count
  },
  set_loading (state, status) {
    state.isLoading = status
  },
  set_userloaded (state, status) {
    state.userloaded = status
  },
  set_page (state, page) {
    state.current_page = page
  },
  set_selection (state, user) {
    state.selected_driver = user
  },
  set_perpage(state, limit) {
    state.per_page = limit
  },
  set_driver (state, data) {
    const user = JSON.stringify(data.message)
    state.driver_details = user
    state.userloaded = true
  },
  set_filters(state, data){
    state.filters = JSON.stringify(data)
  },
  set_sort(state, data){
    state.sort = data
  },
}
export default mutations

const state = {
  completedorders: [],
  total_records: 0,
  current_page: 1,
  selected_order: '',
  isLoading: false,
  order_details: null,
  userloaded: false,
  per_page: 10,
  filters: "{}",
  sort: {"field": "creation", "type": "desc"},
}

export default state

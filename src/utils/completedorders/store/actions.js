import Vue from 'vue'
import api from '@/config/api'

const getUrl = (url) => {
  let urlArray = []
  if (api.api) {
    urlArray.push(api.api)
  } else {
    urlArray.push(api.url)
    if (api.path.prefix) {
      urlArray.push(api.path.prefix)
    }
  }
  if (api.prefix) {
    urlArray.push(api.prefix)
  }
  urlArray.push(url)
  return urlArray.join('/')
}

const actions = {
  get_completedorders ({ commit }, data) {
    commit('set_loading', true)
    return new Promise((resolve) => {
      const path = api.path.completedorders 
      Vue.http.post(getUrl(path),data)
        .then(response => response.json())
        .then((result) => {
          commit('set_completedorders', result)
          resolve()
        })
        .catch(() => {
          commit('set_loading', false)
        })
    })
  },
  getOder ({ commit, state}) {
    commit('set_userloaded', false)
    return new Promise((resolve) => {
      let data = {"name": state.selected_order}
      const path = api.path.getIvoice
      Vue.http.post(getUrl(path), data)
        .then(response => response.json())
        .then((result) => {
          commit('set_order', result)
          resolve()
        })
        .catch(() => {
          commit('set_loading', false)
        })
    })
  },
}

export default actions

const state = {
  promocodes: [],
  total_records: 0,
  current_page: 1,
  selected_promocode: '',
  isLoading: false,
  promocode: '',
  userloaded: false,
  per_page: 10,
  filters: "{}",
  sort: {"field": "creation", "type": "desc"},
}

export default state

const getters = {
  promocodeInfo: state => state.promocodes,
  isloading: state => state.isLoading,
  totalRecords: state => state.total_records,
  promoDetail: state => {
  return JSON.parse(state.promocodes) || {
    active: null,
    email: null,
    name: null,
    }
  },
  isuserloaded: state => state.userloaded,
}

export default getters

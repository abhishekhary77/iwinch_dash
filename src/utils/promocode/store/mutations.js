const mutations = {
  set_promocodes (state, data) {
    const promocode  = data.message.offers
    state.promocodes = promocode
    state.isLoading = false
    state.total_records = data.message.count
  },
  set_loading (state, status) {
    state.isLoading = status
  },
  set_page (state, page) {
    state.current_page = page
  },
  set_selection (state, promocode) {
    state.selected_promocode = promocode
  },
  set_perpage(state, limit) {
    state.per_page = limit
  },
  set_promocode (state, data) {
    const promocode = JSON.stringify(data)
    state.promocode = promocode
    state.userloaded = true
  },
  set_filters(state, data){
    state.filters = JSON.stringify(data)
  },
  set_sort(state, data){
    state.sort = data
  },
}
export default mutations

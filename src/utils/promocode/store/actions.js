import Vue from 'vue'
import api from '@/config/api'


const getUrl = (url) => {
  let urlArray = []
  if (api.api) {
    urlArray.push(api.api)
  } else {
    urlArray.push(api.url)
    if (api.path.prefix) {
      urlArray.push(api.path.prefix)
    }
  }
  if (api.prefix) {
    urlArray.push(api.prefix)
  }
  urlArray.push(url)
  return urlArray.join('/')
}

const actions = {
  get_promocodes({ commit }, data) {
    commit('set_loading', true)
    return new Promise((resolve) => {
      const path = api.path.promocode 
      Vue.http.post(getUrl(path),data)
        .then(response => response.json())
        .then((result) => {
          commit('set_promocodes', result)
          resolve()
        })
        .catch(() => {
          commit('set_loading', false)
        })
    })
  },
  // logout ({ commit } ) {
  //   return new Promise((resolve) => {
  //     const path = auth.paths.logout || 'logout'
  //     Vue.http.post(getUrl(path))
  //       .then(response => response.json())
  //       .then(() => {
  //         commit('logout')
  //         resolve()
  //       })
  //   })
  // },
  // getUser ({ commit }) {
  //   return new Promise((resolve) => {
  //     const path = auth.paths.getUser || 'user'
  //     Vue.http.get(getUrl(path))
  //       .then(response => response.json())
  //       .then((response) => {
  //         if ([
  //           400,
  //           401,
  //           403,
  //         ].includes(response.status)) {
  //           commit('logout')
  //         }
  //         resolve()
  //       })
  //   })
  // },
  // refreshToken ({
  //   commit,
  // }, data) {
  //   const path = auth.paths.refreshToken || 'refresh-token'
  //   Vue.http.post(getUrl(path))
  //     .then(response => response.json())
  //     .then((result) => {
  //       commit('refreshToken', result)
  //     })
  // },
  // editUser ({ commit, dispatch }, data) {
  //   const path = auth.paths.editUser || 'user'
  //   Vue.http.post(getUrl(path), data)
  //     .then(response => {
  //       let result = response.body
  //       commit('editUser', result)
  //     }, (error) => {
  //       dispatch('openAlertBox', [
  //         'alertError',
  //         error.statusText,
  //       ], { root: true })
  //     })
  // },
  // editPassword ({
  //   commit,
  //   dispatch,
  // }, data) {
  //   const path = auth.paths.changePassword || 'password'
  //   Vue.http.post(getUrl(path), data)
  //     .then(response => {
  //       let result = response.body
  //       commit('editPassword', result)
  //     }, (error) => {
  //       dispatch('openAlertBox', [
  //         'alertError',
  //         error.statusText,
  //       ], { root: true })
  //     })
  // },
}

export default actions

const mutations = {
  set_riders (state, data) {
    const riders  = data.message.users
    state.riders = riders
    state.isLoading = false
    state.total_records = data.message.count
  },
  set_loading (state, status) {
    state.isLoading = status
  },
  set_userloaded (state, status) {
    state.userloaded = status
  },
  set_page (state, page) {
    state.current_page = page
  },
  set_selection (state, user) {
    state.selected_user = user
  },
  set_perpage(state, limit) {
    state.per_page = limit
  },
  set_user (state, data) {
    const user = JSON.stringify(data.message)
    state.user_details = user
    state.userloaded = true
  },
  set_filters(state, data){
    state.filters = JSON.stringify(data)
  },
  set_sort(state, data){
    state.sort = data
  },
  set_order(state, data){
    state.completedOrders = data.message.orders
    state.isLoading = false
    state.total_order = data.message.count
  },
}
export default mutations

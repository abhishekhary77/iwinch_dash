const state = {
  riders: [],
  total_records: 0,
  current_page: 1,
  selected_user: '',
  isLoading: false,
  user_details: null,
  userloaded: false,
  per_page: 10,
  filters: "{}",
  sort: {"field": "creation", "type": "desc"},
  completedOrders: [],
  total_order: 0,
}

export default state

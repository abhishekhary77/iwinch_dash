const getters = {
  ridersInfo: state => state.riders,
  isloading: state => state.isLoading,
  totalRecords: state => state.total_records,
  getUsr: state => state.selected_user,
  userDetail: state => {
  return JSON.parse(state.user_details) || {
    active: null,
    email: null,
    name: null,
    }
  },
  isuserloaded: state => state.userloaded,
}

export default getters

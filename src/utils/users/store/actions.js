import Vue from 'vue'
import api from '@/config/api'

const getUrl = (url) => {
  let urlArray = []
  if (api.api) {
    urlArray.push(api.api)
  } else {
    urlArray.push(api.url)
    if (api.path.prefix) {
      urlArray.push(api.path.prefix)
    }
  }
  if (api.prefix) {
    urlArray.push(api.prefix)
  }
  urlArray.push(url)
  return urlArray.join('/')
}

const actions = {
  get_riders ({ commit }, data) {
    commit('set_loading', true)
    return new Promise((resolve) => {
      const path = api.path.users || 'users'
      Vue.http.post(getUrl(path),data)
        .then(response => response.json())
        .then((result) => {
          commit('set_riders', result)
          resolve()
        })
        .catch(() => {
          commit('set_loading', false)
        })
    })
  },
  getUser ({ commit, state}) {
    commit('set_userloaded', true)
    return new Promise((resolve) => {
      let data = {"name": state.selected_user}
      const path = api.path.getUser || 'user'
      Vue.http.post(getUrl(path), data)
        .then(response => response.json())
        .then((result) => {
          commit('set_user', result)
          resolve()
        })
        .catch(() => {
          commit('set_loading', false)
        })
    })
  },
  getIntial({ commit, state}, name) {
    let temp_filter = JSON.parse(state.filters);
    console.log('innnnnnnnnnnnnnnnnnnnnnnnnnnnniiiiiiiiiiiiiiiiiii')
    console.log(temp_filter)
    console.log(temp_filter[name])
    return ''
    // let val = '';
    // try{
    //   temp_filter['']
    // }
  },
  get_orders ({ commit, state}, data) {
    commit('set_loading', true)
    return new Promise((resolve) => {
      const path = api.path.completedorders
      data.filters.customer = state.selected_user
      data.filters = JSON.stringify(data.filters)
      Vue.http.post(getUrl(path),data)
        .then(response => response.json())
        .then((result) => {
          console.log(result)
          commit('set_order', result)
          resolve()
        })
        .catch(() => {
          commit('set_loading', false)
        })
    })
  },
  updateUser({ commit, state}, data) {
    commit('set_userloaded', false)
    return new Promise((resolve) => {
      const path = api.path.updateUser
      console.log('data data data data')
      console.log(data);
      Vue.http.post(getUrl(path),data)
        .then(response => response.json())
        .then((result) => {
          console.log(result)
          commit('set_user', result)
          resolve()
        })
        .catch(() => {
          commit('set_loading', false)
        })
    })
  },
}

export default actions

const mutations = {
  set_ongoingorders (state, data) {
    const ongoingorders  = data.message.orders
    state.ongoingorders = ongoingorders
    state.isLoading = false
    state.total_records = data.message.count
  },
  set_loading (state, status) {
    state.isLoading = status
  },
  set_userloaded (state, status) {
    state.userloaded = status
  },
  set_page (state, page) {
    state.current_page = page
  },
  set_selection (state, order) {
    state.selected_order = order
  },
  set_perpage(state, limit) {
    state.per_page = limit
  },
  set_order(state, data) {
    const user = JSON.stringify(data.message.orders)
    state.order_details = user
    state.userloaded = true
  },
  set_filters(state, data){
    state.filters = JSON.stringify(data)
  },
  set_sort(state, data){
    state.sort = data
  },
}
export default mutations

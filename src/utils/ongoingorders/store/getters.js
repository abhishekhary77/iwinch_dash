const getters = {
  ongoinordersInfo: state => state.ongoingorders,
  isloading: state => state.isLoading,
  totalRecords: state => state.total_records,
  orderDetail: state => {
  return JSON.parse(state.order_details) || {
    active: null,
    email: null,
    name: null,
    }
  },
  isuserloaded: state => state.userloaded,
}

export default getters

import Vue from 'vue'
import api from '@/config/api'

const getUrl = (url) => {
  let urlArray = []
  if (api.api) {
    urlArray.push(api.api)
  } else {
    urlArray.push(api.url)
    if (api.path.prefix) {
      urlArray.push(api.path.prefix)
    }
  }
  if (api.prefix) {
    urlArray.push(api.prefix)
  }
  urlArray.push(url)
  return urlArray.join('/')
}

const actions = {
  get_ongoingorders ({ commit }, data) {
    commit('set_loading', true)
    return new Promise((resolve) => {
      const path = api.path.ongoingorders 
      Vue.http.post(getUrl(path),data)
        .then(response => response.json())
        .then((result) => {
          commit('set_ongoingorders', result)
          resolve()
        })
        .catch(() => {
          commit('set_loading', false)
        })
    })
  },
  getOder ({ commit, state}) {
    commit('set_userloaded', false)
    return new Promise((resolve) => {
      let data = {"name": state.selected_order}
      const path = api.path.getOrder
      Vue.http.post(getUrl(path), data)
        .then(response => response.json())
        .then((result) => {
          commit('set_order', result)
          resolve()
        })
        .catch(() => {
          commit('set_loading', false)
        })
    })
  },
  changeStatus({ commit, state}, data) {
    commit('set_userloaded', false)
    return new Promise((resolve) => {
      const path = api.path.changeStatus
      console.log(path);
      Vue.http.post(getUrl(path),data)
        .then(response => response.json())
        .then((result) => {
          console.log(result)
          commit('set_order', result)
          resolve()
        })
        .catch(() => {
          commit('set_loading', false)
        })
    })
  },

}

export default actions

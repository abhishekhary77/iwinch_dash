import Vue from 'vue'
import i18n from './locales/index'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import './plugins/http'
import './plugins/ie'
import './plugins/custom/'
import App from './App.vue'
import  Ionic from '@ionic/vue'
import '@ionic/core/css/core.css'
import '@ionic/core/css/ionic.bundle.css'
// import  IonicVueRouter  from '@ionic/vue'
import 'beautify-scrollbar/dist/index.css';
// import 'vue-good-table/dist/vue-good-table.css'
import VueGoodTable from './plugins/vue-good-table'
// import VueGoodTablePlugin from 'vue-good-table';
import * as firebase from "firebase";

Vue.use(Ionic)
// Vue.use(IonicVueRouter)
Vue.use(LiquorTree)
Vue.use(VueGoodTable);

Vue.http.interceptors.push((request, next) => {
  if (localStorage.getItem('iwinch_token')) {
    request.headers.set('Authorization', 'Basic ' + localStorage.getItem('iwinch_token'))
    console.log(localStorage.getItem('iwinch_token'))
  }
  next(response => {
    if (response.status === 400 || response.status === 401 || response.status === 403) {
      console.log(response.status)
      store.commit('auth/logout');
      router.push({ path: '/login' });
    }
  })
})

const firebaseConfig = {
  apiKey: "AIzaSyC1ZeQjZcSoeE_zRueu-qZjK86QEl-Snfs",
  authDomain: "iwinch.firebaseapp.com",
  databaseURL: "https://iwinch.firebaseio.com",
  projectId: "iwinch",
  storageBucket: "iwinch.appspot.com",
  messagingSenderId: "804973225101",
  appId: "1:804973225101:web:4b2e796e5f74463184aec4",
  measurementId: "G-SVZKVVHJ82"
};
firebase.initializeApp(firebaseConfig);


new Vue({
  i18n,
  router,
  store,
  vuetify,
  LiquorTree,
  VueGoodTable,
  render: h => h(App),
}).$mount('#app')

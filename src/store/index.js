import Vue from 'vue'
import Vuex from 'vuex'

// main store
import state from './state'
import getters from './getters'
import mutations from './mutations'
import actions from './actions'
// crud
import crud from '@/utils/crud/store/'
// auth
import auth from '@/utils/auth/store/'
// app
import app from '@/utils/app/store/'
//riders
import users from '@/utils/users/store/'
//drivers
import drivers from '@/utils/drivers/store/'
//ongoingorders
import ongoingorders from '@/utils/ongoingorders/store/'
//completedorders
import completedorders from '@/utils/completedorders/store/'
//approvals
import approvals from '@/utils/approvals/store/'
//promocode
import promocodes from '@/utils/promocode/store/'


import customModules from '@/config/store-modules'

const mainModules = { crud, auth, app, users, drivers, ongoingorders, completedorders, approvals, promocodes }
const modules = Object.assign(customModules, mainModules)

Vue.use(Vuex)

export default new Vuex.Store({
  modules,
  state,
  getters,
  mutations,
  actions,
})

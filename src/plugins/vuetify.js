import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Vue from 'vue'
// import { TiptapVuetifyPlugin } from 'tiptap-vuetify'
import Vuetify from 'vuetify/lib'
import main from '@/config/main'
import vuetifyLocales from '@/locales/vuetify'
import 'tiptap-vuetify/dist/main.css'
import store from '@/store/index'
import { Touch } from 'vuetify/lib/directives'

Vue.use(Vuetify, {
  directives: {
    Touch
  }
})

const vuetify = new Vuetify({
  theme: {
    dark: false,
    themes: {
      light: main.theme,
    },
  },
  icons: {
    iconfont: 'md',
  },
  lang: {
    locales: vuetifyLocales || {},
    current: store.getters.lng,
  },
  directives: {
    Touch
  }
})

// Vue.use(TiptapVuetifyPlugin, {
//   vuetify,
//   iconsGroup: 'md',
// })

export default vuetify

// import '@mdi/font/css/materialdesignicons.min.css' // Ensure you are using css-loader
// import Vue from 'vue'
// import Vuetify from 'vuetify'
// import 'vuetify/dist/vuetify.min.css'

// Vue.use(Vuetify)

// export default new Vuetify({
//   icons: {
//     // https://vuetifyjs.com/en/customization/icons
//     iconfont: 'mdi' // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4'
//   }
// })
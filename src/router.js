import Vue from 'vue'
import Router from 'vue-router'
import Crud from './routes/Crud.vue'
import App from './routes/App.vue'
import Login from './routes/Login.vue'
// import Logged from './routes/POS.vue'
import Dashboard from './routes/dashboard_1.vue'
import Promocode from './routes/promocode.vue'
import Settings from './routes/Settings.vue'
import Approvals from './routes/approvals.vue'
import Orders from './routes/orders.vue'
// import Products from './routes/products.vue'
// import Dashboard from './routes/dashboard.vue'
import store from '@/store/'
import Users from './routes/users.vue'
import Drivers from './routes/drivers.vue'
import Userdetails from './routes/usr.vue'
import Driverdetails from './routes/driver1.vue' 
import Orderdetails from './routes/order1.vue'
import OrderInvoice from './routes/invoice.vue'
import Approvaldetails from './routes/approvals1.vue'
import Ongoingorders from './routes/ongoingorders.vue'
import Completedorders from './routes/completedorders.vue'
import AddPromocode from './routes/Addpromo.vue'
import ViewPromocode from './routes/Viewpromo.vue'
import Paymentrequest from './routes/Paymentrequest.vue'
// import Navbar from './utils/navbar/components/navbar.vue'
Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/crud',
      name: 'crud',
      component: Crud,
    },
    {
      path: '/app',
      name: 'app',
      component: App,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (auth) {
          next('/')
        } else {
          next()
        }
      },
    },
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },

    {
      path: '/promocode',
      name: 'Promocode',
      component: Promocode,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },

    {
      path: '/approvals',
      name: 'Approvals',
      component: Approvals,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/orders',
      name: 'Orders',
      component: Orders,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
      // child: [
      //   {
      //     path: '/ongoingorders',
      //     name: 'OngoingOrders',
      //     component: Ongoingorders
      //   },
      //   ],
  
    },

    {
      path: '/drivers',
      name: 'Drivers',
      component: Drivers,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/users',
      name: 'Users',
      component: Users,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/userdetails',
      name: 'Userdetails',
      component: Userdetails,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/driverdetails',
      name: 'Driverdetails',
      component: Driverdetails,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/orderdetails',
      name: 'Orderdetails',
      component: Orderdetails,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/invoicedetails',
      name: 'Invoicedetails',
      component: OrderInvoice,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/approvaldetails',
      name: 'Approvaldetails',
      component: Approvaldetails,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/ongoingorders',
      name: 'Ongoingorders',
      component: Ongoingorders,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/completedorders',
      name: 'Completedorders',
      component: Completedorders,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/addpromocode',
      name: 'AddPromocode',
      component: AddPromocode,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/viewpromocode',
      name: 'ViewPromocode',
      component: ViewPromocode,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    },
    {
      path: '/paymentrequest',
      name: 'Paymentrequest',
      component: Paymentrequest,
      beforeEnter: (to, from, next) => {
        var auth = localStorage.getItem('iwinch_token')
        if (!auth) {
          store.commit('auth/logout')
          next('/login')
        } else {
          next()
        }
      },
    }

  ],
})

export default router

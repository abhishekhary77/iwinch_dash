export default {
  url: 'http://35.226.233.132:8080',
  // url: 'http://10.15.1.202:8080',
  // url: 'http://localhost:8080',
  // url: 'http://35.226.233.132',
  path: {
    prefix: 'api/method',
    storage: 'storage',
    upload: 'files/file-upload',

    completedorders: 'orders.api_admin.list_invoices',
    getIvoice: 'orders.api_admin.get_invoice',

    drivers: 'profiles.api_admin.list_drivers',
    approvals: 'profiles.api_admin.list_approvals',
    getDriver: 'profiles.api_admin.get_driver',
    updateDriver: 'profiles.api_admin.update_provider_name',
    approveDriver: 'profiles.api_admin.approve_driver',

    ongoingorders: 'orders.api_admin.list_orders',
    getOrder: 'orders.api_admin.get_order',
    changeStatus: 'orders.api_admin.change_status',

    promocode: 'discounts.api_admin.list_offers',

    users: 'profiles.api_admin.list_riders',
    getUser: 'profiles.api_admin.get_user',
    updateUser: 'profiles.api_admin.update_customer_name'
    
  },
}

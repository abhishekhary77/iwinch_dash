

const required = msg => v => !!v || msg
const validEmail = msg => v => (/.+@.+\..+/.test(v) || !v ) || msg
const minLen = l => v => (v && v.length >= l) || `min. ${l} Characters`
const maxLen = l => v => (v && v.length <= l) || `max. ${l} Characters`
const toUpper = ( {value} ) => value && value.toUpperCase() 

const help  = {
    required,
    validEmail,
    minLen,
    maxLen,
    toUpper,
    acceptImg:'image/*', 
}

export default help